defmodule QuobisHotel.MixProject do
  use Mix.Project

  def project do
    [
      app: :quobis_hotel,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {QuobisHotel.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5.0-rc.2", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:typed_struct, "~> 0.2.1"},
      {:uuid, "~> 1.1"},
      {:ratatouille, "~> 0.5.0"}
    ]
  end

  defp aliases do
    [
      test: "test --no-start"
    ]
  end
end
