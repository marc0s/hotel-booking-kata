# QuobisHotel

An elixir implementation of the [Corporate hotel booking kata](https://katalyst.codurance.com/corporate-hotel-booking).

## Installation

Clone the repository and fetch all its dependencies with `mix deps.get`.

## Running the tests

Easy as running `mix test --trace`

