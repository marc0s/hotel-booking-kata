defmodule QuobisHotel.Boundary.BookingService do
  alias QuobisHotel.Types.Error
  alias QuobisHotel.Core.RestrictedBookings
  alias QuobisHotel.Boundary.{CompanyService, HotelService, PolicyService}
  use GenServer

  # Client API (from the Kata spec)
  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def book(employee_id, hotel_id, room_type, check_in, check_out) do
    GenServer.call(
      __MODULE__,
      {:book, employee_id, hotel_id, room_type, check_in, check_out},
      :infinity
    )
  end

  def all_bookings() do
    GenServer.call(__MODULE__, {:all_bookings}, :infinity)
  end

  # Server API
  def init(bookings) when is_list(bookings) do
    {:ok, bookings}
  end

  def init(_bookings), do: {:error, %Error{code: :bad_data, message: "bookings must be a list"}}

  def handle_call({:book, employee_id, hotel_id, room_type, check_in, check_out}, _from, bookings) do
    hotel = get_hotel_by_id(hotel_id)

    if hotel == nil do
      {:reply, {:error, %Error{code: :not_found, message: "No such hotel"}}, bookings}
    else
      {:ok, policies} = PolicyService.all_policies()
      {:ok, employees} = CompanyService.all_employees()

      case RestrictedBookings.book(
             %{
               bookings: bookings,
               hotels: [hotel],
               policies: policies,
               employees: employees
             },
             employee_id,
             hotel_id,
             room_type,
             check_in,
             check_out
           ) do
        {:ok, booking} -> {:reply, {:ok, booking}, bookings ++ [booking]}
        {:error, %Error{} = error} -> {:reply, {:error, error}, bookings}
      end
    end
  end

  def handle_call({:all_bookings}, _from, bookings) do
    {:reply, {:ok, bookings}, bookings}
  end

  defp get_hotel_by_id(hotel_id) do
    case HotelService.findHotelById(hotel_id) do
      {:error, %Error{code: :not_found, message: _}} -> nil
      {:ok, hotel} -> hotel
    end
  end
end
