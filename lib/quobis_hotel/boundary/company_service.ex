defmodule QuobisHotel.Boundary.CompanyService do
  use GenServer
  alias QuobisHotel.Types.Employee
  alias QuobisHotel.Core.Companies

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def add_employee(company_id, employee_id) do
    GenServer.call(__MODULE__, {:add_employee, company_id, employee_id}, :infinity)
  end

  def all_employees() do
    GenServer.call(__MODULE__, {:all_employees}, :infinity)
  end

  def init(employees) when is_list(employees) do
    {:ok, employees}
  end

  def init(_employees), do: {:error, "employees must be a list"}

  def handle_call({:add_employee, company_id, employee_id}, _from, employees) do
    employee = %Employee{
      company_id: company_id,
      employee_id: employee_id
    }

    new_state = Companies.add_employee(employees, employee)
    {:reply, {:ok}, new_state}
  end

  def handle_call({:all_employees}, _from, employees) do
    {:reply, {:ok, employees}, employees}
  end
end
