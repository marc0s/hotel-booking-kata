defmodule QuobisHotel.Boundary.PolicyService do
  use GenServer

  alias QuobisHotel.Core.Policies
  alias QuobisHotel.Types.{CompanyPolicy, EmployeePolicy}

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def add_company_policy(company_id, room_types) do
    GenServer.call(__MODULE__, {:add_company_policy, company_id, room_types}, :infinity)
  end

  def add_employee_policy(employee_id, room_types) do
    GenServer.call(__MODULE__, {:add_employee_policy, employee_id, room_types}, :infinity)
  end

  def all_policies() do
    GenServer.call(__MODULE__, {:all_policies}, :infinity)
  end

  def init(policies) when is_list(policies) do
    {:ok, policies}
  end

  def init(_policies), do: {:error, "policies must be a list"}

  def handle_call({:add_company_policy, company_id, room_types}, _from, policies) do
    policy = %CompanyPolicy{
      company_id: company_id,
      room_types: room_types
    }

    new_state = Policies.add_company_policy(policies, policy)

    {:reply, {:ok}, new_state}
  end

  def handle_call({:add_employee_policy, employee_id, room_types}, _from, policies) do
    policy = %EmployeePolicy{
      employee_id: employee_id,
      room_types: room_types
    }

    new_state = Policies.add_employee_policy(policies, policy)

    {:reply, {:ok}, new_state}
  end

  def handle_call({:all_policies}, _from, policies) do
    {:reply, {:ok, policies}, policies}
  end
end
