# credo:disable-for-this-file Credo.Check.Readability.VariableNames
# credo:disable-for-this-file Credo.Check.Readability.FunctionNames
defmodule QuobisHotel.Boundary.HotelService do
  alias QuobisHotel.Core.Hotels
  alias QuobisHotel.Types.{Error, Hotel}
  use GenServer

  # Client API
  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def findHotelById(hotelId) do
    GenServer.call(__MODULE__, {:find_by_id, hotelId}, :infinity)
  end

  def addHotel(hotelId, hotelName) do
    GenServer.call(__MODULE__, {:add_hotel, hotelId, hotelName}, :infinity)
  end

  def setRoom(hotelId, number, roomType) do
    GenServer.call(__MODULE__, {:set_room, hotelId, number, roomType}, :infinity)
  end

  # Server API
  def init(hotels) when is_list(hotels) do
    {:ok, hotels}
  end

  def init(_hotels), do: {:error, %Error{code: :bad_data, message: "hotels must be a list"}}

  def handle_call({:find_by_id, hotel_id}, _from, hotels) do
    hotel = Hotels.find_by_id(hotels, hotel_id)

    if hotel != nil do
      {:reply, {:ok, hotel}, hotels}
    else
      {:reply, {:error, %Error{code: :not_found, message: "No such hotel"}}, hotels}
    end
  end

  def handle_call({:add_hotel, hotelId, hotelName}, _from, hotels) do
    hotel = %Hotel{hotel_id: hotelId, hotel_name: hotelName, rooms: %{}}

    try do
      state = Hotels.add(hotels, hotel)
      {:reply, {:ok}, state}
    rescue
      ArgumentError ->
        {:reply, {:error, %Error{code: :bad_data, message: "Failed to add hotel"}}, hotels}
    end
  end

  def handle_call({:set_room, hotelId, number, roomType}, _from, hotels) do
    hotel = Hotels.find_by_id(hotels, hotelId)

    if hotel != nil do
      state = Hotels.set_room(hotels, hotelId, number, roomType)
      {:reply, {:ok, state}, state}
    else
      {:reply, {:error, %Error{code: :not_found, message: "No such hotel"}}, hotels}
    end
  end
end
