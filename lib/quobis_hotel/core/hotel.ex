defmodule QuobisHotel.Core.Hotels do
  alias QuobisHotel.Types.{Hotel, Room}

  @spec find_by_id(list(Hotel.t()), String.t()) :: Hotel.t() | nil
  def find_by_id(hotels, hotel_id) do
    Enum.find(hotels, nil, fn hotel -> hotel.hotel_id == hotel_id end)
    |> Hotel.from_struct()
  end

  @spec add([], Hotel.t()) :: list(Hotel.t())
  def add(hotels, hotel) when hotels == [] do
    [hotel]
  end

  @spec add(nonempty_list(Hotel.t()), Hotel.t()) :: list(Hotel.t())
  def add(hotels, hotel) do
    case Enum.find(hotels, nil, fn h -> h.hotel_id == hotel.hotel_id end) do
      nil -> hotels ++ [hotel]
      _ -> raise ArgumentError, message: "already exists"
    end
  end

  @spec set_room([], String.t(), Room.room_number(), Room.room_type()) ::
          list(Hotel.t())
  def set_room(hotels, _hotel_id, _number, _room_type) when hotels == [] do
    []
  end

  @spec set_room(
          nonempty_list(Hotel.t()),
          String.t(),
          Room.room_number(),
          Room.room_type()
        ) ::
          list(Hotel.t())
  def set_room(hotels, hotel_id, number, roomType) do
    hotels
    |> Enum.map(fn hotel -> update_room(hotel, hotel_id, number, roomType) end)
  end

  @spec update_room(Hotel.t(), String.t(), Room.room_number(), Room.room_type()) ::
          Hotel.t()
  defp update_room(hotel, hotel_id, number, roomType) do
    if hotel.hotel_id != hotel_id do
      hotel
    else
      %Hotel{hotel | rooms: Map.put(hotel.rooms, number, roomType)}
    end
  end
end
