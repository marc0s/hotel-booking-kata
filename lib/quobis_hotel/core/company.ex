defmodule QuobisHotel.Core.Companies do
  alias QuobisHotel.Types.Employee
  @spec add_employee([], Employee.t()) :: list(Employee.t())
  def add_employee(employees, employee) when employees == [] do
    [employee]
  end

  @spec add_employee(nonempty_list(Employee.t()), Employee.t()) :: list(Employee.t())
  def add_employee(employees, employee) do
    case Enum.member?(employees, employee) do
      true -> raise ArgumentError, message: "already exists"
      false -> employees ++ [employee]
    end
  end

  @spec delete_employee([], Employee.t()) :: []
  def delete_employee(employees, _employee) when employees == [] do
    []
  end

  @spec delete_employee(nonempty_list(Employee.t()), Employee.t()) :: list(Employee.t())
  def delete_employee(employees, employee) do
    Enum.reject(employees, fn e ->
      e.employee_id == employee.employee_id and e.company_id == employee.company_id
    end)
  end
end
