defmodule QuobisHotel.Core.RestrictedBookings do
  alias QuobisHotel.Core.{SimpleBookings, Hotels, Policies}
  alias QuobisHotel.Types.{Booking, CompanyPolicy, Employee, EmployeePolicy, Error, Hotel, Room}

  @spec book(
          %{
            bookings: list(Booking.t()),
            hotels: list(Hotel.t()),
            policies: list(CompanyPolicy.t() | EmployeePolicy.t()),
            employees: list(Employee.t())
          },
          String.t(),
          String.t(),
          Room.room_type(),
          Date.t(),
          Date.t()
        ) :: {:ok, Booking.t()} | {:error, Error.t()}
  def book(
        %{bookings: _bookings, hotels: _hotels, policies: _policies, employees: _employees} =
          state,
        employee_id,
        hotel_id,
        room_type,
        check_in,
        check_out
      ) do
    cond do
      valid_hotel?(state.hotels, hotel_id) == false ->
        {:error, %Error{code: :not_found, message: "The given hotel does not exist"}}

      valid_room_type?(state.hotels, hotel_id, room_type) == false ->
        {:error,
         %Error{code: :bad_data, message: "The given hotel has no room of type #{room_type}"}}

      booking_allowed?(state.policies, state.employees, employee_id, room_type) == false ->
        {:error, %Error{code: :not_allowed, message: "You cannot book this room type"}}

      free_rooms?(state.bookings, state.hotels, hotel_id, room_type, check_in, check_out) == false ->
        {:error, %Error{code: :conflict, message: "No rooms available"}}

      true ->
        SimpleBookings.book(employee_id, hotel_id, room_type, check_in, check_out)
    end
  end

  @spec valid_hotel?(list(Hotel.t()), String.t()) :: boolean()
  defp valid_hotel?(hotels, hotel_id) do
    Hotels.find_by_id(hotels, hotel_id) != nil
  end

  @spec valid_room_type?(list(Hotel.t()), String.t(), Room.room_type()) :: boolean()
  defp valid_room_type?(hotels, hotel_id, room_type) do
    case Hotels.find_by_id(hotels, hotel_id) do
      nil ->
        false

      hotel ->
        Enum.find(hotel.rooms, false, fn {_key, value} ->
          value == room_type
        end)
    end
  end

  @spec booking_allowed?(
          list(CompanyPolicy.t() | EmployeePolicy.t()),
          list(Employee.t()),
          String.t(),
          Room.room_type()
        ) :: boolean()
  defp booking_allowed?(policies, employees, employee_id, room_type) do
    Policies.booking_allowed?(policies, employees, employee_id, room_type)
  end

  @spec free_rooms?(
          list(Booking.t()),
          list(Hotel.t()),
          String.t(),
          Room.room_type(),
          Date.t(),
          Date.t()
        ) :: boolean()
  defp free_rooms?(bookings, hotels, hotel_id, room_type, check_in, check_out) do
    hotel = Hotels.find_by_id(hotels, hotel_id)
    rooms_count = Enum.count(Enum.filter(hotel.rooms, fn {_key, value} -> value == room_type end))
    bookings_for_dates = bookings_for_dates(bookings, room_type, check_in, check_out)
    Enum.count(bookings_for_dates) < rooms_count
  end

  defp bookings_for_dates(bookings, room_type, check_in, check_out) do
    Enum.filter(bookings, fn booking ->
      booking.room_type == room_type and gte(booking.check_out, check_in) and
        lte(booking.check_in, check_out)
    end)
  end

  defp gte(date1, date2) do
    case Date.compare(date1, date2) do
      :lt -> false
      _ -> true
    end
  end

  defp lte(date1, date2) do
    case Date.compare(date1, date2) do
      :gt -> false
      _ -> true
    end
  end
end
