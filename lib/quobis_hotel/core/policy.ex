defmodule QuobisHotel.Core.Policies do
  alias QuobisHotel.Types.{CompanyPolicy, Employee, EmployeePolicy, Room}
  @type policies :: CompanyPolicy.t() | EmployeePolicy.t()

  @spec add_company_policy([], CompanyPolicy.t()) :: list(policies())
  def add_company_policy(policies, %CompanyPolicy{} = policy) when policies == [] do
    [policy]
  end

  @spec add_company_policy(nonempty_list(policies()), CompanyPolicy.t()) :: list(policies())
  def add_company_policy(policies, %CompanyPolicy{} = policy) do
    upsert_company_policy(policies, policy)
  end

  @spec add_employee_policy([], EmployeePolicy.t()) :: list(policies())
  def add_employee_policy(policies, %EmployeePolicy{} = policy) when policies == [] do
    [policy]
  end

  @spec add_employee_policy(policies(), EmployeePolicy.t()) :: list(policies())
  def add_employee_policy(policies, %EmployeePolicy{} = policy) do
    upsert_employee_policy(policies, policy)
  end

  @spec booking_allowed?(list(policies()), list(Employee.t()), String.t(), Room.room_type()) ::
          boolean()
  def booking_allowed?(policies, _employees, _employee_id, _room_type)
      when policies == [] do
    true
  end

  @spec booking_allowed?(list(policies()), list(Employee.t()), String.t(), Room.room_type()) ::
          boolean()
  def booking_allowed?(policies, _employees, _employee_id, room_type)
      when length(policies) == 1 do
    case policies |> Enum.find(fn p -> Enum.member?(p.room_types, room_type) end) do
      nil -> false
      _ -> true
    end
  end

  @spec booking_allowed?(list(policies()), list(Employee.t()), String.t(), Room.room_type()) ::
          boolean()
  def booking_allowed?(policies, employees, employee_id, room_type) when length(policies) > 1 do
    policies
    |> find_matching_policy(employees, employee_id)
    |> booking_allowed?(employee_id, employees, room_type)
  end

  def find_matching_policy(policies, employees, employee_id) do
    case match_employee(policies, employee_id) do
      [] -> match_company(policies, employees, employee_id)
      p -> p
    end
  end

  def all_policies(policies), do: policies

  defp match_employee(policies, employee_id) do
    Enum.filter(policies, fn p -> !!Map.get(p, :employee_id) and p.employee_id == employee_id end)
  end

  defp match_company(policies, employees, employee_id) do
    employee = find_employee(employees, employee_id)
    find_company_policy(policies, employee)
  end

  defp find_employee(employees, employee_id) do
    Enum.find(employees, fn e -> !!Map.get(e, :employee_id) and e.employee_id == employee_id end)
  end

  defp find_company_policy([], _employee), do: []

  defp find_company_policy(_policies, employee) when is_nil(employee) do
    []
  end

  defp find_company_policy(policies, employee) do
    Enum.filter(policies, fn p ->
      !!Map.get(p, :company_id) and p.company_id == employee.company_id
    end)
  end

  defp upsert_employee_policy(policies, policy) do
    case policies
         |> Enum.find_index(fn p ->
           !!Map.get(p, :employee_id) and p.employee_id == policy.employee_id
         end) do
      nil -> policies ++ [policy]
      idx -> List.replace_at(policies, idx, policy)
    end
  end

  defp upsert_company_policy(policies, policy) do
    case policies
         |> Enum.find_index(fn p ->
           !!Map.get(p, :company_id) and p.company_id == policy.company_id
         end) do
      nil -> policies ++ [policy]
      idx -> List.replace_at(policies, idx, policy)
    end
  end
end
