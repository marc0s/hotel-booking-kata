defmodule QuobisHotel.Core.SimpleBookings do
  alias QuobisHotel.Types.{Booking, Room}

  @spec book(String.t(), String.t(), Room.room_type(), Date.t(), Date.t()) ::
          {:ok, Booking.t()} | {:error, String.t()}
  def book(employee_id, hotel_id, room_type, check_in, check_out) do
    case out_before_in?(check_in, check_out) do
      true ->
        {:error, "You cannot book a room with a check_out date prior to check_in"}

      false ->
        {:ok, Booking.new(employee_id, hotel_id, room_type, check_in, check_out)}
    end
  end

  @spec out_before_in?(Date.t(), Date.t()) :: boolean()
  defp out_before_in?(checkin, checkout) do
    Date.compare(checkin, checkout) != :lt
  end
end
