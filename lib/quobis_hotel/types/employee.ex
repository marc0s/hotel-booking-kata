defmodule QuobisHotel.Types.Employee do
  alias __MODULE__

  use TypedStruct

  typedstruct do
    field(:employee_id, String.t())
    field(:company_id, String.t())
  end

  @spec new(String.t(), String.t()) :: Employee.t()
  def new(employee_id, company_id) do
    %__MODULE__{
      employee_id: employee_id,
      company_id: company_id
    }
  end
end
