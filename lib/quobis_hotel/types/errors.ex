defmodule QuobisHotel.Types.Error do
  @moduledoc """
  A basic Error struct for specifying application errors
  """
  use TypedStruct

  @type error_code :: :already_exists | :bad_data | :conflict | :not_allowed | :not_found

  typedstruct do
    field(:code, error_code(), enforce: true)
    field(:message, String.t(), enforce: true)
  end
end
