defmodule QuobisHotel.Types.EmployeePolicy do
  alias __MODULE__
  alias QuobisHotel.Types.Room

  use TypedStruct

  typedstruct do
    field(:employee_id, String.t())
    field(:room_types, list(Room.room_type()))
  end

  @spec new(String.t(), list(Room.room_type())) :: EmployeePolicy.t()
  def new(employee_id, room_types) do
    %__MODULE__{
      employee_id: employee_id,
      room_types: room_types
    }
  end
end
