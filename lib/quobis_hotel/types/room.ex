defmodule QuobisHotel.Types.RoomType do
  @type room_type :: :single | :double | :suite
  @type room_number :: String.t()
end
