defmodule QuobisHotel.Types.Hotel do
  alias __MODULE__
  alias QuobisHotel.Types.Room
  use TypedStruct

  typedstruct do
    field(:hotel_id, String.t())
    field(:hotel_name, String.t())
    field(:rooms, %{Room.room_number() => Room.room_type()})
  end

  @spec new(String.t(), String.t(), %{Room.room_number() => Room.room_type()}) :: Hotel.t()
  def new(hotel_id, hotel_name, rooms) do
    %__MODULE__{
      hotel_id: hotel_id,
      hotel_name: hotel_name,
      rooms: rooms
    }
  end

  @spec from_struct(%{
          hotel_id: String.t(),
          hotel_name: String.t(),
          rooms: %{Room.room_number() => Room.room_type()}
        }) :: Hotel.t()
  def from_struct(%{hotel_id: hotel_id, hotel_name: hotel_name, rooms: rooms} = h)
      when h != nil do
    Hotel.new(hotel_id, hotel_name, rooms)
  end

  def from_struct(h) when h == nil do
    nil
  end
end
