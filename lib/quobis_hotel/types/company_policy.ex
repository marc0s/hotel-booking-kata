defmodule QuobisHotel.Types.CompanyPolicy do
  alias __MODULE__
  alias QuobisHotel.Types.Room

  use TypedStruct

  typedstruct do
    field(:company_id, String.t())
    field(:room_types, list(Room.room_type()))
  end

  @spec new(String.t(), list(Room.room_type())) :: CompanyPolicy.t()
  def new(company_id, room_types) do
    %__MODULE__{
      company_id: company_id,
      room_types: room_types
    }
  end
end
