defmodule QuobisHotel.Types.Booking do
  alias __MODULE__
  alias QuobisHotel.Types.Room

  use TypedStruct

  typedstruct do
    field(:id, String.t())
    field(:employee_id, String.t())
    field(:hotel_id, String.t())
    field(:room_type, String.t())
    field(:check_in, Date)
    field(:check_out, Date)
  end

  @spec new(String.t(), String.t(), Room.room_type(), Date.t(), Date.t()) :: Booking.t()
  def new(employee_id, hotel_id, room_type, check_in, check_out) do
    booking_id = UUID.uuid4()

    %__MODULE__{
      id: booking_id,
      employee_id: employee_id,
      hotel_id: hotel_id,
      room_type: room_type,
      check_in: check_in,
      check_out: check_out
    }
  end
end
