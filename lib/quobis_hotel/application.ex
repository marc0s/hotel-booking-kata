defmodule QuobisHotel.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    QuobisHotel.Supervisor.start_link(name: QuobisHotel.Supervisor)
  end
end
