defmodule QuobisHotelTest.Policies do
  use ExUnit.Case

  alias QuobisHotel.Core.Policies
  alias QuobisHotel.Types.{CompanyPolicy, Employee, EmployeePolicy}

  test "No duplicate company or employee policies are allowed" do
    company_policy_one = %CompanyPolicy{
      company_id: "1",
      room_types: [:single]
    }

    company_policy_two = %CompanyPolicy{
      company_id: "2",
      room_types: [:single]
    }

    policies = Policies.add_company_policy([], company_policy_one)
    assert policies == [company_policy_one]
    policies = Policies.add_company_policy(policies, company_policy_two)
    assert policies == [company_policy_one, company_policy_two]

    updated_policy_one = %CompanyPolicy{
      company_id: "1",
      room_types: [:single, :double]
    }

    policies = Policies.add_company_policy(policies, updated_policy_one)
    assert policies == [updated_policy_one, company_policy_two]

    employee_policy = %EmployeePolicy{employee_id: "1", room_types: [:double]}
    policies = Policies.add_employee_policy(policies, employee_policy)
    policies = Policies.add_employee_policy(policies, employee_policy)
    assert policies == [updated_policy_one, company_policy_two, employee_policy]
  end

  test "If neither employee nor company policies exist, the employee should be allowed to book any room" do
    employees = [
      %Employee{employee_id: "1", company_id: "1"},
      %Employee{employee_id: "2", company_id: "2"}
    ]

    assert Policies.booking_allowed?([], employees, "1", :suite) == true

    assert Policies.booking_allowed?(
             [
               %EmployeePolicy{employee_id: "1", room_types: [:double, :suite]},
               %CompanyPolicy{company_id: "1", room_types: [:single]}
             ],
             employees,
             "2",
             :superior
           ) == true
  end

  test "Employee policies take precedence over company policies. If there is a policy for an employee, the policy should be respected regardless of what the company policy (if any) says. " do
    employees = [
      %Employee{employee_id: "1", company_id: "1"},
      %Employee{employee_id: "2", company_id: "1"}
    ]

    assert Policies.booking_allowed?(
             [
               %EmployeePolicy{employee_id: "1", room_types: [:double, :suite]},
               %CompanyPolicy{company_id: "1", room_types: [:single]}
             ],
             employees,
             "1",
             :suite
           ) == true

    assert Policies.booking_allowed?(
             [
               %EmployeePolicy{employee_id: "1", room_types: [:double, :suite]},
               %CompanyPolicy{company_id: "1", room_types: [:single]}
             ],
             employees,
             "1",
             :superior
           ) == false
  end

  test "If an employee policy does not exist, the company policy should be checked" do
    employees = [
      %Employee{employee_id: "1", company_id: "1"},
      %Employee{employee_id: "2", company_id: "1"}
    ]

    assert Policies.booking_allowed?(
             [
               %CompanyPolicy{company_id: "1", room_types: [:suite]}
             ],
             employees,
             "1",
             :suite
           ) == true

    assert Policies.booking_allowed?(
             [
               %CompanyPolicy{company_id: "1", room_types: [:suite]}
             ],
             employees,
             "1",
             :superior
           ) == false
  end

  test "All policies can be retrieved" do
    policies = [
      %EmployeePolicy{employee_id: "1", room_types: [:double, :suite]},
      %CompanyPolicy{company_id: "1", room_types: [:single]}
    ]

    assert Policies.all_policies(policies) == policies
  end
end
