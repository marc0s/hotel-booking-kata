defmodule QuobisHotelTest.RestrictedBooking do
  use ExUnit.Case

  alias QuobisHotel.Types.{Booking, CompanyPolicy, Employee, EmployeePolicy, Error, Hotel}
  alias QuobisHotel.Core.{RestrictedBookings, Companies, Policies}

  test "booking: validate if the hotel exists and room type is provided by the hotel" do
    hotels = [
      %Hotel{
        hotel_id: "1",
        hotel_name: "Quobis",
        rooms: %{}
      }
    ]

    employee_id = "alice"
    room_type = :suite
    check_in = ~D[2020-09-29]
    check_out = ~D[2020-09-30]

    {:error, error} =
      RestrictedBookings.book(
        %{bookings: [], hotels: hotels, policies: [], employees: []},
        employee_id,
        "0",
        room_type,
        check_in,
        check_out
      )

    assert error == %Error{code: :not_found, message: "The given hotel does not exist"}

    {:error, error} =
      RestrictedBookings.book(
        %{bookings: [], hotels: hotels, policies: [], employees: []},
        employee_id,
        "1",
        :double,
        check_in,
        check_out
      )

    assert error == %Error{
             code: :bad_data,
             message: "The given hotel has no room of type double"
           }
  end

  test "Verify if booking is allowed according to the booking policies defined, if any" do
    hotels = [
      %Hotel{
        hotel_id: "1",
        hotel_name: "Quobis",
        rooms: %{"1" => :suite, "2" => :single, "3" => :superior}
      }
    ]

    employee_id = "alice"
    company_id = "quobis"

    alice = %Employee{
      company_id: company_id,
      employee_id: employee_id
    }

    check_in = ~D[2020-10-16]
    check_out = ~D[2020-10-18]
    alice_room_types = [:single, :double]
    quobis_room_types = [:single, :double, :suite]
    quobis_employees = Companies.add_employee([], alice)

    alice_policy = %EmployeePolicy{
      employee_id: employee_id,
      room_types: alice_room_types
    }

    quobis_policy = %CompanyPolicy{
      company_id: company_id,
      room_types: quobis_room_types
    }

    policies = Policies.add_company_policy([], quobis_policy)
    policies = policies ++ Policies.add_employee_policy(policies, alice_policy)

    assert {:error, %Error{code: :not_allowed, message: "You cannot book this room type"}} ==
             RestrictedBookings.book(
               %{
                 bookings: [],
                 hotels: hotels,
                 policies: policies,
                 employees: quobis_employees
               },
               employee_id,
               "1",
               :suite,
               check_in,
               check_out
             )

    assert {:error, %Error{code: :not_allowed, message: "You cannot book this room type"}} ==
             RestrictedBookings.book(
               %{
                 bookings: [],
                 hotels: hotels,
                 policies: policies,
                 employees: quobis_employees
               },
               employee_id,
               "1",
               :superior,
               check_in,
               check_out
             )

    {:ok, booking} =
      RestrictedBookings.book(
        %{bookings: [], hotels: hotels, policies: policies, employees: quobis_employees},
        employee_id,
        "1",
        :single,
        check_in,
        check_out
      )

    assert booking.id != nil
  end

  test "Booking should only be allowed if there is at least one room type available during the whole booking period." do
    hotels = [
      %Hotel{
        hotel_id: "1",
        hotel_name: "Quobis",
        rooms: %{
          "1" => :suite,
          "2" => :single,
          "3" => :single,
          "4" => :single,
          "5" => :single,
          "6" => :double,
          "7" => :double
        }
      }
    ]

    bookings = [
      %Booking{
        id: "b1",
        employee_id: "alice",
        hotel_id: "1",
        room_type: :single,
        check_in: ~D[2020-11-01],
        check_out: ~D[2020-11-03]
      },
      %Booking{
        id: "b2",
        employee_id: "alice",
        hotel_id: "1",
        room_type: :single,
        check_in: ~D[2020-11-02],
        check_out: ~D[2020-11-03]
      },
      %Booking{
        id: "b3",
        employee_id: "alice",
        hotel_id: "1",
        room_type: :double,
        check_in: ~D[2020-11-05],
        check_out: ~D[2020-11-06]
      },
      %Booking{
        id: "b4",
        employee_id: "alice",
        hotel_id: "1",
        room_type: :suite,
        check_in: ~D[2020-11-02],
        check_out: ~D[2020-11-08]
      }
    ]

    employee_id = "alice"
    company_id = "quobis"

    alice = %Employee{
      company_id: company_id,
      employee_id: employee_id
    }

    quobis_employees = Companies.add_employee([], alice)

    assert {:error, %Error{code: :conflict, message: "No rooms available"}} ==
             RestrictedBookings.book(
               %{bookings: bookings, hotels: hotels, policies: [], employees: quobis_employees},
               employee_id,
               "1",
               :suite,
               ~D[2020-11-03],
               ~D[2020-11-04]
             )

    assert {:ok, %Booking{}} =
             RestrictedBookings.book(
               %{bookings: bookings, hotels: hotels, policies: [], employees: quobis_employees},
               employee_id,
               "1",
               :single,
               ~D[2020-11-03],
               ~D[2020-11-04]
             )
  end
end
