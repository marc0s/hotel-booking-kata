defmodule QuobisHotelTest.HotelService do
  use ExUnit.Case
  alias QuobisHotel.Types.{Error, Hotel}
  alias QuobisHotel.Boundary.HotelService

  defp initial_hotels(),
    do: [
      %Hotel{
        hotel_id: "1",
        hotel_name: "tested hotel #1",
        rooms: %{"1" => :suite, "2" => :double}
      }
    ]

  setup %{} do
    start_supervised({HotelService, initial_hotels()})
    :ok
  end

  describe "HotelManager" do
    test "hotels can be found by their id" do
      assert HotelService.findHotelById("1") ==
               {:ok,
                %Hotel{
                  hotel_id: "1",
                  hotel_name: "tested hotel #1",
                  rooms: %{"1" => :suite, "2" => :double}
                }}

      assert HotelService.findHotelById("2") ==
               {:error, %Error{code: :not_found, message: "No such hotel"}}
    end

    test "addHotel should throw an exception when the hotel ID already exists or create the hotel otherwise" do
      new_hotel = %Hotel{
        hotel_id: "2",
        hotel_name: "tested hotel #2",
        rooms: %{}
      }

      {:ok} = HotelService.addHotel(new_hotel.hotel_id, new_hotel.hotel_name)

      assert {:error, %Error{code: :bad_data, message: "Failed to add hotel"}} ==
               HotelService.addHotel("1", "already defined")
    end

    test "setRoom can set hotel rooms" do
      assert HotelService.setRoom("1", "0", :suite) ==
               {:ok,
                [
                  %Hotel{
                    hotel_id: "1",
                    hotel_name: "tested hotel #1",
                    rooms: %{"0" => :suite, "1" => :suite, "2" => :double}
                  }
                ]}

      assert HotelService.setRoom("2", "0", :suite) ==
               {:error, %Error{code: :not_found, message: "No such hotel"}}
    end
  end
end
