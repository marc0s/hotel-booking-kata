defmodule QuobisHotelTest.PolicyService do
  use ExUnit.Case
  alias QuobisHotel.Types.{CompanyPolicy, EmployeePolicy}
  alias QuobisHotel.Boundary.PolicyService

  defp state(),
    do: [
      %CompanyPolicy{
        company_id: "1",
        room_types: [:single, :double, :suite]
      },
      %EmployeePolicy{
        employee_id: "1",
        room_types: [:single, :double]
      }
    ]

  setup %{} do
    start_supervised({PolicyService, state()})
    :ok
  end

  describe "PolicyService" do
    test "policies can be added" do
      assert {:ok} == PolicyService.add_company_policy("2", [:single])
      assert {:ok} == PolicyService.add_employee_policy("2", [:suite])
    end

    test "can fetch all policies" do
      assert PolicyService.all_policies() == {:ok, state()}
      assert {:ok} == PolicyService.add_company_policy("2", [:single])
      assert {:ok} == PolicyService.add_employee_policy("2", [:suite])

      assert PolicyService.all_policies() ==
               {:ok,
                state() ++
                  [
                    %CompanyPolicy{company_id: "2", room_types: [:single]},
                    %EmployeePolicy{employee_id: "2", room_types: [:suite]}
                  ]}
    end
  end
end
