defmodule QuobisHotelTest.Bookings do
  use ExUnit.Case
  alias QuobisHotel.Core.SimpleBookings

  test "booking: should contain a unique ID, employee_id, hotel_id, room_type, check_in and check_out" do
    employee_id = "alice"
    hotel_id = "1"
    room_type = :suite
    check_in = ~D[2020-09-29]
    check_out = ~D[2020-09-30]

    {:ok, first_booking} =
      SimpleBookings.book(employee_id, hotel_id, room_type, check_in, check_out)

    {:ok, second_booking} =
      SimpleBookings.book(employee_id, hotel_id, room_type, check_in, check_out)

    assert first_booking.id != nil
    assert second_booking.id != first_booking.id
  end

  test "booking: Check out date must be at least one day after the check in date" do
    employee_id = "alice"
    hotel_id = "1"
    room_type = :suite
    check_in = ~D[2020-09-29]
    check_out = ~D[2020-09-29]
    {:error, message} = SimpleBookings.book(employee_id, hotel_id, room_type, check_in, check_out)
    assert message == "You cannot book a room with a check_out date prior to check_in"
  end
end
