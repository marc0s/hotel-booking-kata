defmodule QuobisHotelTest.Hotels do
  use ExUnit.Case

  alias QuobisHotel.Types.Hotel
  alias QuobisHotel.Core.Hotels

  test "hotel can be added" do
    hotel = %Hotel{
      hotel_id: "1",
      hotel_name: "first hotel",
      rooms: []
    }

    assert Hotels.add([], hotel) == [hotel]

    hotel_two = %Hotel{
      hotel_id: "2",
      hotel_name: "second hotel",
      rooms: []
    }

    assert Hotels.add([hotel], hotel_two) == [hotel] ++ [hotel_two]
  end

  test "same hotel cannot be added twice" do
    hotel = %Hotel{
      hotel_id: "2",
      hotel_name: "first hotel renamed",
      rooms: []
    }

    assert_raise ArgumentError, "already exists", fn ->
      Hotels.add([hotel], %Hotel{hotel | hotel_name: "ola k ase"})
    end
  end

  test "hotel rooms can be set" do
    hotel = %Hotel{
      hotel_id: "1",
      hotel_name: "first",
      rooms: %{}
    }

    room_number = "1"
    room_type = :single

    assert Hotels.set_room([], "1", room_number, room_type) == []

    assert Hotels.set_room([hotel], "2", room_number, room_type) == [hotel]

    assert Hotels.set_room([hotel], "1", room_number, room_type) == [
             %Hotel{hotel | rooms: %{"1" => :single}}
           ]

    assert Hotels.set_room([hotel], "1", room_number, :double) == [
             %Hotel{hotel | rooms: %{"1" => :double}}
           ]

    assert Hotels.set_room([%Hotel{hotel | rooms: %{"1" => :double}}], "1", "2", :suite) == [
             %Hotel{
               hotel
               | rooms: %{
                   "1" => :double,
                   "2" => :suite
                 }
             }
           ]
  end
end
