defmodule QuobisHotelTest.CompanyService do
  use ExUnit.Case
  alias QuobisHotel.Types.Employee
  alias QuobisHotel.Boundary.CompanyService

  defp state(),
    do: []

  setup %{} do
    start_supervised({CompanyService, state()})
    :ok
  end

  describe "CompanyService" do
    test "can add a new employee" do
      assert {:ok} == CompanyService.add_employee("grand_hotel", "alice")
    end

    test "can fetch all companies" do
      CompanyService.add_employee("grand_hotel", "alice")
      CompanyService.add_employee("grand_hotel", "bob")

      assert CompanyService.all_employees() ==
               {:ok,
                [
                  %Employee{company_id: "grand_hotel", employee_id: "alice"},
                  %Employee{company_id: "grand_hotel", employee_id: "bob"}
                ]}
    end
  end
end
