defmodule QuobisHotelTest.BookingService do
  use ExUnit.Case
  alias QuobisHotel.Types.{CompanyPolicy, Employee, EmployeePolicy, Error, Hotel}
  alias QuobisHotel.Boundary.{BookingService, CompanyService, HotelService, PolicyService}

  defp state(),
    do: []

  defp hotels(),
    do: [
      %Hotel{
        hotel_id: "quobis",
        hotel_name: "Quobis Grand Hotel",
        rooms: %{"1" => :single, "2" => :suite, "3" => :single}
      }
    ]

  defp policies(),
    do: [
      %EmployeePolicy{employee_id: "1", room_types: [:single]},
      %CompanyPolicy{company_id: "1", room_types: [:suite]}
    ]

  defp employees(),
    do: [
      %Employee{employee_id: "1", company_id: "1"}
    ]

  setup %{} do
    start_supervised({CompanyService, employees()})
    start_supervised({PolicyService, policies()})
    start_supervised({HotelService, hotels()})
    start_supervised({BookingService, state()})
    :ok
  end

  test "Booking should only be allowed if there is at least one room type available during the whole booking period." do
    check_in = ~D[2020-10-30]
    check_out = ~D[2020-11-01]

    {:ok, _booking} = BookingService.book("alice", "quobis", :suite, check_in, check_out)

    assert {:error, %Error{code: :conflict, message: "No rooms available"}} ==
             BookingService.book("alice", "quobis", :suite, check_in, check_out)
  end

  test "Keep track of all bookings" do
    check_in = ~D[2020-10-30]
    check_out = ~D[2020-11-02]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    check_in = ~D[2020-11-02]
    check_out = ~D[2020-11-03]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    check_in = ~D[2020-11-02]
    check_out = ~D[2020-11-02]

    assert {:error, %Error{code: :conflict, message: "No rooms available"}} ==
             BookingService.book("alice", "quobis", :single, check_in, check_out)
  end

  test "Hotel rooms can be booked many times as long as there are no conflicts with the dates" do
    check_in = ~D[2020-10-30]
    check_out = ~D[2020-11-02]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    check_in = ~D[2020-11-02]
    check_out = ~D[2020-11-03]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    check_in = ~D[2020-11-10]
    check_out = ~D[2020-11-11]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    check_in = ~D[2020-11-20]
    check_out = ~D[2020-11-30]
    {:ok, _booking} = BookingService.book("alice", "quobis", :single, check_in, check_out)

    {:ok, bookings} = BookingService.all_bookings()
    assert Enum.count(bookings) == 4
  end

  test "Bookings cannot be placed on non-existing hotels" do
    check_in = ~D[2020-11-20]
    check_out = ~D[2020-11-30]

    assert {:error, %Error{code: :not_found, message: "No such hotel"}} =
             BookingService.book("alice", "404", :single, check_in, check_out)
  end
end
