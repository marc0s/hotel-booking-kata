defmodule QuobisHotelTest.Companies do
  use ExUnit.Case

  alias QuobisHotel.Core.Companies

  test "Employees should not be duplicated." do
    employees = []

    employee = %{
      company_id: "c1",
      employee_id: "e1"
    }

    second_employee = %{
      company_id: "c1",
      employee_id: "e2"
    }

    assert Companies.add_employee(employees, employee) == [employee]
    assert Companies.add_employee([employee], second_employee) == [employee, second_employee]

    assert_raise ArgumentError, "already exists", fn ->
      Companies.add_employee([employee], employee)
    end
  end

  test "Employees can be deleted" do
    employees = [
      %{
        company_id: "1",
        employee_id: "1"
      },
      %{
        company_id: "2",
        employee_id: "1"
      }
    ]

    assert Companies.delete_employee(employees, %{company_id: "1", employee_id: "1"}) == [
             %{company_id: "2", employee_id: "1"}
           ]

    assert Companies.delete_employee([], %{company_id: "1", employee_id: "1"}) == []
  end
end
